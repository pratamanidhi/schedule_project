<?php
error_reporting(1);
session_start();
require_once 'libs/config.php'; 
require_once 'libs/phpfunction.php'; 

?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Expires" content="Mon, 26 Jul 1997 05:00:00 GMT">
	<meta http-equiv="Pragma" content="no-cache">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
	<meta http-equiv="refresh" content="6000; url=home.php">

	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="description" content="stock-management">
	<meta name="author" content="@vincentGrerry">
	<title>.:: Stock Management System - LEE SOON SENG PLASTIC INDUSTRIES SDN BHD</title>

	<!-- Bootstrap core CSS -->
	<link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="libs/bootstrap/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" >
	
	<!-- DataTables -->
	<link href="libs/bootstrap/extend/bootstrap-dataTable/css/dataTables.bootstrap.css" rel="stylesheet" >
	<link href="libs/bootstrap/extend/bootstrap-dataTable/css/buttons.dataTables.min.css" rel="stylesheet" >
	<link href="libs/bootstrap/extend/bootstrap-dataTable/ext/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />

	<!-- bootstrap datepicker -->
	<link href="libs/bootstrap/extend/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" >

	<!-- Custom styles for this template -->
	<!-- <link href="libs/bootstrap/extend/select2/css/select2.css" rel="stylesheet"> -->
	<link href="libs/bootstrap/extend/select2/css/select2-bootstrap.min.css" rel="stylesheet">
	<link href="libs/bootstrap/extend/select2/css/select2.min.css" rel="stylesheet">

	<link href="libs/common/css/style.css" rel="stylesheet">

	<!-- Theme style -->
	<link href="libs/dist/css/modern-AdminLTE.min.css" rel="stylesheet" type="text/css" />

	<!-- ../adminlte Skins. Choose a skin from the css/skins 
	folder instead of downloading all of them to reduce the load. -->

	<!-- jQuery v1.11.3 -->
	<script src="libs/common/js/jquery.min.js"></script>

	<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
	<!--[if lt IE 9]><script src="assets/common/js/ie8-responsive-file-warning.js"></script><![endif]-->
	<script src="libs/common/js/ie-emulation-modes-warning.js"></script>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition modern-skin-dark fixed sidebar-mini">
	<!-- <b>aaa</b> -->
	<div class='loading'></div>

	<div id="full-wrapper">
		<header class="main-header">
			<!-- Logo -->
			<a href="<?= base_url ?>" class="logo">
			<!-- mini logo for sidebar mini 50x50 pixels -->
			<span class="logo-mini"></span>
			<!-- logo for regular state and mobile devices -->
			<span class="logo-lg">
				<img src="<?= base_url ?>libs/common/img/logo.jpeg" alt="logo" class="img-circle" width="70%"></b>
				<!-- <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;<b>AI Smartual</b> -->
			</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div id="navbar" class="navbar-collapse collapse dropdown">
						<ul class="nav navbar-nav navbar-right">
							<li><a data-toggle="modal" href="#myModal" title="Sign In"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a></li>
						</ul>
					</div><!--/.nav-collapse -->
				</div>
			</nav>
		</header>



		<footer>
			<div class="navbar navbar-inverse navbar-fixed-bottom" role="navigation" style="min-height:10px !important;">
				<div class="container">
					<div id="footer-warp">
						<p style="color:#fff;" class="text-center">&copy; schedule 2022</p>    
					</div>
				</div>
			</div>
		</footer>

		<!-- Login Modals Item -->
		<div style="display: none;margin-top:150px !important;" id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h4 class="modal-title" id="myModalLabel"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign In</h4>
					</div>

					<div class="modal-body">
						<div class="panel panel-info">
							<div class="panel-body">
								<input type="hidden" id="action" name="action" value="loginApp" /> 
								<div class="form-group">
									<label for="inputID" class="sr-only">User ID</label>
									<input type="text" id="inputID" name="inputID" class="form-control" placeholder="User ID" required autofocus autocomplete="off">
								</div>
								<div class="form-group">
									<label for="inputPassword" class="sr-only">Password</label>
									<input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password" require autocomplete="off"d>
								</div>
							</div>
							<div class="panel-footer">
								<div class="text-right">
									<button class="btn btn-sm btn-primary" type="button" id="signin"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign In</button>&nbsp;
									<button class="btn btn-sm btn-default" type="reset" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>

	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- Bootstrap Script -->
	<script src="libs/bootstrap/js/bootstrap.min.js"></script>

	<!-- Bootstrap Extended -->
	<script src="libs/bootstrap/extend/bootbox/bootbox.min.js"></script>
	
	<!-- DataTables -->
	<script src="libs/bootstrap/extend/bootstrap-dataTable/js/jquery.dataTables.js"></script>
	<script src="libs/bootstrap/extend/bootstrap-dataTable/js/dataTables.bootstrap.js"></script>
	<script src="libs/bootstrap/extend/bootstrap-dataTable/js/jszip.min.js"></script>
	<script src="libs/bootstrap/extend/bootstrap-dataTable/js/pdfmake.min.js"></script>
	<script src="libs/bootstrap/extend/bootstrap-dataTable/js/buttons.html5.min.js"></script>
	<script src="libs/bootstrap/extend/bootstrap-dataTable/js/vfs_fonts.js"></script>
	<script src="libs/bootstrap/extend/bootstrap-dataTable/js/dataTables.buttons.min.js"></script>
	<script src="libs/bootstrap/extend/bootstrap-dataTable/js/buttons.print.min.js"></script>
	<script src="libs/bootstrap/extend/bootstrap-dataTable/ext/Responsive/js/dataTables.responsive.min.js"></script>

	<!-- Datepicker -->
	<script src="libs/bootstrap/extend/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

	<!-- Select2 -->
	<!-- <script src="libs/bootstrap/extend/select2/js/select2.full.js"></script> -->
	<script src="libs/bootstrap/extend/select2/js/select2.min.js"></script>
	
	<!-- jQuery Validate -->
	<script src="libs/plugins/jquery.validate/jquery.validate.min.js"></script>
	<script src="libs/plugins/jquery.validate/additional-methods.min.js"></script>

	<!-- AdminLTE App -->
	<script src="libs/dist/js/app.min.js"></script>

	<!-- Slimscroll -->
	<script src="libs/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>

	<!-- FastClick -->
	<script src='libs/plugins/fastclick/fastclick.min.js'></script>
	
	<!-- Lodash -->
	<script src="libs/common/js/lodash.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="libs/common/js/ie10-viewport-bug-workaround.js"></script>

	<!-- Custom Onload Script -->
	<script type="text/javascript" src="libs/common/js/script.js"></script>

	<script type="text/javascript">
		(function($) {
			$(document).ready(function(){
				$.ajaxPrefilter(function( options, original_Options, jqXHR ) {
					options.async = true;
				});

				var url = window.location;
				// for sidebar menu entirely but not cover treeview
				$('ul.sidebar-menu a').filter(function() {
					return this.href == url;
				}).parent().addClass('active');
				// for treeview
				$('ul.treeview-menu a').filter(function() {
					return this.href == url;
				}).closest('.treeview').addClass('active');

				$("#signin").click(function() {
					var userId		= $("#inputID").val();
					var password   = $("#inputPassword").val();

					if(userId == ''){
						bootbox.alert("Sorry, User ID must be filled !!", function() {
							$("#username").focus();
						});
					}
					else if(password == ''){
						bootbox.alert('Sorry, Password must be filled !!', function() {
							$("#password").focus();
						});
					}
					else{
						$.ajax({  
							type    : 'POST',
							url     : '<?=base_url?>libs/proses.php',
							data    : {'userId' :  userId,'password' : password, 'action' : 'loginApp'},
							dataType: "json",
							success : function(data) {
								if(data.error == false){
									location.href='home.php?ref=home';
								}
								else{
									bootbox.alert(data.message);
								}
							},  
							error : function() {  
								bootbox.alert(data.message);  
							}  
						});
					}
					return false;
				}); 
 			});
		})( jQuery );
	</script>
</body>
</html>
