<?php
error_reporting(0);
session_start();

function guid(){
	if (function_exists('com_create_guid')){
		return com_create_guid();
	}
	else{
		mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
		$charid = md5(uniqid(rand(), true));
		$hyphen = chr(45);// "-"
		$uuid = 
			substr($charid, 0, 8).$hyphen
			.substr($charid, 8, 4).$hyphen
			.substr($charid,12, 4).$hyphen
			.substr($charid,16, 4).$hyphen
			.substr($charid,20,12);
		return $uuid;
	}
}

function convertText($type, $text){
	switch($type){
		case 'ucwords' : $convertText = ucwords(strtolower(trim($text))); break;
		case 'upper' 	: $convertText = strtoupper(trim($text)); break;
		case 'lower' 	: $convertText = strtolower(trim($text)); break;
	}
	return $convertText;
}

function MakeDirectory($dir, $mode = 0755){
	if (is_dir($dir) || @mkdir($dir,$mode)) return TRUE;
	if (!MakeDirectory(dirname($dir),$mode)) return FALSE;
	return @mkdir($dir,$mode);
}

function writeLog($line, $filename, $message, $sql = '', $logfile = '') {
	if($logfile == '') {
		$logfile = dirname(__DIR__) ."/LogFiles/Log_".date('Ymd').".txt";
	}

	if(!empty($message) || $message != ''){
		MakeDirectory('LogFiles');
		$fd 	= fopen($logfile, "a");
		$str 	= "[" . date("d-m-Y H:i:s", mktime()) . "] Error at line " . ($line-1) . " in file " . $filename ." : ". $message ." \n[SQL] : ".$sql; 
		fwrite($fd, $str . "\n");
		fclose($fd);
	}
}

function breadcrumb($page, $parent = ''){
	switch($page){
		case 'manage-employee'		: $content = "Manage Employee"; break;
		case 'add-new-employee'		: $content = "Add New Employee"; break;
		case 'manage-location'		: $content = "Manage Location"; break;
		case 'add-new-location'		: $content = "Add New Location"; break;
		case 'list-product-items'	: $content = "List of Product Items"; break;
		case 'input-qty-product'	: $content = "Input Quantity"; break;
		case 'purchase-order'		: $content = "Purchase Order"; break;
		case 'stock-report'			: $content = "Stock Report"; break;
		case 'summary-report'		: $content = "Summary Report"; break;
		case 'change-pass'			: $content = "Change Password"; break;
		case 'update-input-stock' 	: $content = "Update Stock Item"; break;
		case 'manage-user' 			: $content = "Manage User"; break;
		case 'add-user-app'			: $content = "Add User"; break;
		case 'home'						: $content = ""; break;
		default 							: $content = "Input Quantity"; break;
	}

	switch($parent){
		case 'master-data'		: $contentParent = "Master Data"; break;
		case 'product' 			: $contentParent = "Product"; break;
		case 'reporting'		: $contentParent = "Reporting"; break;
		default 				: $contentParent = "Product"; break;
		
	}
	
	echo '<div class="row">';
		echo '<div class="col-lg-12 col-xs-12">';
			echo '<ul class="breadcrumb">';
				echo '<li><a href="'.base_url.'home.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>';
				if(!empty($parent)){
					echo '<li class=".nav-divider">'.$contentParent.'</li>';
				}
				echo '<li class=".nav-divider">'.$content.'</li>';
			echo '</ul>';
		echo '</div>';
	echo '</div>';
}

function randomString($length = 6) {
	$str = "";
	$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
	$max = count($characters) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max);
		$str .= $characters[$rand];
	}
	return $str;
}

function login($form_ID, $form_password){
	global  $connDB;
	
	if($form_password == "" or $form_ID == ""){
		$result = array('message' => 'Sorry, please re-check the User ID / password that you have entered !!', 'error' => true);
	}
	else{
		$sql = "select a.user_id, a.username_id, a.username, a.password, a.user_access, a.status, b.access_name from 
		schedule_user_management a, schedule_user_access b where a.user_access = b.access_id and a.username_id = '".$form_ID."' and a.password = '".$form_password."'";
		$exe = mysqli_query($connDB, $sql);
		$row = mysqli_fetch_array($exe, MYSQLI_ASSOC);
		writeLog(__LINE__, __FILE__, mysqli_error($connDB));
		
		$kdUser 			= trim($row['user_id']);
		$uid 				= trim($row['username_id']);
		$username			= trim($row['username']);
		$passwd  			= trim($row['password']);
		$userKdAccess 		= trim($row['user_access']);
		$userNmAccess 		= trim($row['access_name']);
		$isActive 			= trim($row['status']);
		$encryptedUserID 	= base64_encode($kdUser.'_'.$uid);
		
		if($exe == true){
			if($isActive ==  'active'){
				if(($uid != '') && ($form_ID == $uid) && ($form_password == $passwd)){
					$_SESSION['token'] 		= $encryptedUserID; 
					$_SESSION['kdUser']		= $kdUser; 
					$_SESSION['userid']		= $uid; 
					$_SESSION['username'] 	= $username;
					$_SESSION['kdAccess']	= $userKdAccess;
					$_SESSION['nmAccess']	= $userNmAccess;

					$sql = "update schedule_user_management set online_status = '1' where user_id = ".$kdUser;
					$exe = mysqli_query($connDB, $sql);
					writeLog(__LINE__, __FILE__, mysqli_error($connDB));

					$result = array('error' => false);
				}
				else 
				{
					$result = array('message' => 'Sorry, the User Id / Password you entered is not correct !!', 'error' => $kdUser);
				}
			}
			else{
				$result = array('message' => '<p align="left">Sorry, your account is <b>.$isActive.</b> !!.<br> Please contact the Administrator to activate your account. Thank you.</p>', 'error' => true);
			}
		}
		else{
			$result = array('message' => 'Sorry, you are not registered yet !! !!', 'error' => true);
		}
	}
	echo json_encode($result);
}

