// JavaScript Document
var currentDirectory = window.location.pathname.split('/').slice(0, -1).join('/');
var base_url = window.location.protocol + "//" + window.location.host + currentDirectory + '/';

$(function() {
	
	$('.loading').fadeOut('slow');

	$(".datepicker").datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      todayHighlight: true,
   });

	$('a.page-scroll').click(function() {
	
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		  var target = $(this.hash);
		  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		  if (target.length) {
			$('html,body').animate({
			  scrollTop: target.offset().top - 70
			}, 900);
			return false;
		  }
		}
	});

	$(".select2").select2({
		allowClear: true,
		containerCssClass : "show-hide"
	});

	var dataTable = $("#dataTable, .dataTable");
	dataTable.DataTable({
		responsive  : true,
		fixedHeader : true,
		columnDefs : [
			{
				"targets"   : 'no-sort',
				"orderable" : false
			},
		],
		lengthMenu 		: [[20, 50, 100, -1], [20, 50, 100, "All"]],
		responsive      : true,
		ordering        : true,
		scrollCollapse  : false,
		paging          : true,
		searching       : true,
		dom            	: 'frltip'	
	});

});

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Chrome, Safari and Opera
    document.documentElement.scrollTop = 0; // For IE and Firefox
} 

function isnumber(evt){ 
	var code 		= evt.keyCode || evt.which;
  var charCode 	= (code) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 45 || charCode > 57) || charCode === 9)
  return false;	
}
