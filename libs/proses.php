<?php
session_start();
error_reporting(0);
clearstatcache();
header("Pragma: no-cache" );
header("Cache-Control: no-cache");


require("./config.php");
require("./phpfunction.php");
require('./plugins/excelReader/excel_reader2.php');

switch($_POST['action']){
	case 'add_account' :
		if(empty($_POST['isEdit']) || $_POST['isEdit'] == ''){
			$sql = "insert into aismartual_user_management (userid, password, username, email, kd_access, status, online_status, datecreate, resetcode)
				values ('".$_POST['userid']."', '".base64_encode(md5(trim($_POST['password'])))."', '".$_POST['username']."', '".$_POST['email']."', '".$_POST['userAccess']."', 
				'".$_POST['isActive']."', '0', NOW(), '')";
		}
		else{
			$password = (!empty($_POST['password']) || $_POST['password'] != '') ? "password = '".base64_encode(md5(trim($_POST['password'])))."'," : "";
			$sql = "update aismartual_user_management set ".$password." username = '".$_POST['username']."', email = '".$_POST['email']."', 
			kd_access = '".$_POST['userAccess']."', status = '".$_POST['isActive']."' where kd_user = '".$_POST['isEdit']."'";	
		}
		$exe = mysqli_query($connDB, $sql);
		writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);
		$result = ($exe == true) ? array('message' => 'New user '.((empty($_POST['isEdit']) || $_POST['isEdit'] == '') ? 'added' : 'updated').' successfully !!', 'error' => false) : array('message' => 'Error: Data failed to save !!', 'error' => true); 
		echo json_encode($result);	
	break;
	case 'update_stock_item' :
		extract($_POST);
		$sql = "update aismartual_product_list set product_old_code = '".trim($old_code)."', product_category = '".trim($category)."', 
		product_status = '".trim($status)."', product_uom = '".trim($uom)."', product_total_pack = '".trim($total_pack)."', update_date = NOW()
		where product_id = '".$isEdit."'";
		$exe = mysqli_query($connDB, $sql);
		writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);

		$qry = "update aismartual_stock_qty set stock_qty = '".trim($qty)."', update_date = NOW()
		where product_id = '".$isEdit."'";
		$exec = mysqli_query($connDB, $qry);

		writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);
		$result = ($exe == true && $exec == true) ? array('message' => 'Data has been updated successfully !!', 'error' => false) : array('message' => 'Error: Data Failed to update !!', 'error' => true);	
		echo json_encode($result);
	break;
	case 'changePass' :
		$sql = "update aismartual_user_management set password = '".base64_encode(md5(trim($_POST['newPass'])))."' 
				where kd_user = '".$_SESSION['kdUser']."'";
		$exe = mysqli_query($connDB, $sql);
		writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);
		$result = ($exe == true) ? array('message' => 'Password has been changed successfully, and please login again  !!', 'error' => false) : array('message' => 'Error: Password failed to update !!', 'error' => true); 
		echo json_encode($result);	
	break;
	case 'verify-items' :
		for($x=0; $x < count($_POST['data']); $x++){
			$sql = "update aismartual_stock_qty set is_verified = '1', verified_date = NOW() 
			where stock_qty_uuid = '".$_POST['data'][$x]."'";
			$exe = mysqli_query($connDB, $sql);
			writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);
		}
		$result = ($exe == true) ? array('message' => 'Data has been validated successfully !!', 'error' => false) : array('message' => 'Error: Data Failed to delete !!', 'error' => true);	
		echo json_encode($result);
	break;
	case 'getQty' :
		$sql = "select product_total_pack from aismartual_product_list where product_list_uuid = '".$_POST['product']."'";
		$exe = mysqli_query($connDB, $sql);
		$row = mysqli_fetch_array($exe);
		writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);

		$result = ($exe == true) ? array('pack' => (float)$row['product_total_pack'], 'error' => false) : array('error' => true);	
		echo json_encode($result);
	break;
	case 'add_qty' :
		extract($_POST);
		$sql = "select apl.product_id, asq.stock_qty_uuid from aismartual_product_list apl left join aismartual_stock_qty asq on apl.product_id = asq.product_id where apl.product_list_uuid = '".$product."'	and (apl.is_archived = '0' or asq.is_verified = '0' or asq.is_verified is null)";
		$exe = mysqli_query($connDB, $sql);
		$row = mysqli_fetch_array($exe);
		writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);

		$sql = "insert into aismartual_stock_qty(stock_qty_uuid, product_id, stock_qty, employee_uuid, location_uuid, entry_date) 
		values('".guid()."', '".$row['product_id']."', '".$qty."', '".$employee."', '".$location."', NOW())";
		$exe = mysqli_query($connDB, $sql);
		writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);
		
		$_SESSION['name_entry'] = $employee;
		$_SESSION['location_entry'] = $location;

		$result = ($exe == true) ? array('message' => 'Insert QTY successfully !!', 'error' => false) : array('message' => 'Error: Data failed to save !!', 'error' => true); 
		echo json_encode($result);	
	break;
	case 'archive-product-items' :
		for($x=0; $x < count($_POST['data']); $x++){
			$sql = "update aismartual_product_list set is_archived = '1', archived_date = NOW() where product_list_uuid = '".$_POST['data'][$x]."'";
			$exe = mysqli_query($connDB, $sql);
			writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);
		}
		$result = ($exe == true) ? array('message' => 'Data has been archived successfully !!', 'error' => false) : array('message' => 'Error: Data Failed to archive !!', 'error' => true);	
		echo json_encode($result);
	break;
	case 'import_product_list' :
		$allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
		if(in_array($_FILES['file-excel']['type'], $allowedFileType)){
			$data = new Spreadsheet_Excel_Reader($_FILES['file-excel']['tmp_name']);
			$hasildata = $data->rowcount($sheet_index = 0);
			for ($i = 2; $i <= $hasildata; $i++) {
				$sql = "select product_list_uuid from aismartual_product_list	
				where lower(product_id) = '".strtolower(trim($data->val($i, 3)))."' and lower(product_category) = '".strtolower(trim($data->val($i, 2)))."' and lower(product_status) = '".strtolower(trim($data->val($i, 1)))."' and lower(product_old_code) = '".strtolower(trim($data->val($i, 4)))."' and lower(product_uom) = '".strtolower(trim($data->val($i, 5)))."'";
				$exe = mysqli_query($connDB, $sql);
				writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);
				$result = mysqli_fetch_array($exe, MYSQLI_ASSOC);

				if($result['product_list_uuid'] == ''){
					$sql = "insert into aismartual_product_list (product_list_uuid, product_id, product_old_code, product_category, product_status, product_uom, product_total_pack, upload_date) 
					values ('".guid()."', '".trim($data->val($i, 3))."', '".trim($data->val($i, 4))."', 
					'".trim($data->val($i, 2))."', '".trim($data->val($i, 1))."', '".trim($data->val($i, 5))."',
					'".str_replace("-", 0, trim($data->val($i, 6)))."', NOW())";
				}
				else{
					$sql = "update aismartual_product_list set product_total_pack = '".str_replace("-", 0, trim($data->val($i, 6)))."', 
					upload_date = NOW() where product_list_uuid = '".$result['product_list_uuid']."'";
				}

				$exe = mysqli_query($connDB, $sql);
				writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);
			}
			$result = ($exe == true) ? array('message' => 'Product Items imported successfully !!', 'error' => false) : array('message' => 'Error: Data failed to save !!', 'error' => true); 
		}
		else{
			$result = array('message' => 'Error: Invalid File Type !', 'error' => true);
		}
		echo json_encode($result);
	break;
	case 'import_location' :
		$allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
		if(in_array($_FILES['file-excel']['type'], $allowedFileType)){
			$data = new Spreadsheet_Excel_Reader($_FILES['file-excel']['tmp_name']);
			$hasildata = $data->rowcount($sheet_index = 0);
			for ($i = 2; $i <= $hasildata; $i++) {
				$sql = "select location_code from aismartual_location	
				where lower(location_code) = '".strtolower(trim($data->val($i, 2)))."' and lower(location_name) = '".strtolower(trim($data->val($i, 3)))."'";
				$exe = mysqli_query($connDB, $sql);
				writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);
				$result = mysqli_fetch_array($exe, MYSQLI_ASSOC);

				if($result['location_uuid'] == ''){
					$sql = "insert into aismartual_location (location_uuid, location_code, location_name, status, created_date) 
					values ('".guid()."', '".convertText('upper', trim($data->val($i, 2)))."', '".convertText('upper', trim($data->val($i, 3)))."', 'active', NOW())";
					$exe = mysqli_query($connDB, $sql);
					writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);
				}
			}
			$result = ($exe == true) ? array('message' => 'New Location imported successfully !!', 'error' => false) : array('message' => 'Error: Data failed to import !!', 'error' => true); 
		}
		else{
			$result = array('message' => 'Error: Invalid File Type !', 'error' => true);
		}
		echo json_encode($result);
	break;
	case 'add_location' :
		extract($_POST);
		if(empty($isEdit) || $isEdit == ''){
			$sql = "insert into aismartual_location (location_uuid, location_code, location_name, status, created_date) 
			values ('".guid()."', '".convertText('upper', $location_code)."', '".convertText('upper', $location_name)."', '".$isActive."', NOW())";
		}
		else{
			$sql = "update aismartual_location set location_code = '".convertText('upper', $location_code)."', location_name = '".convertText('upper', $location_name)."', status = '".$isActive."' where location_uuid = '".$isEdit."'";	
		}
		
		$exe = mysqli_query($connDB, $sql);
		writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);
		$result = ($exe == true) ? array('message' => 'New Location '.((empty($isEdit) || $isEdit == '') ? 'added' : 'updated').' successfully !!', 'error' => false) : array('message' => 'Error: Data failed to save !!', 'error' => true); 
		echo json_encode($result);	
	break;
	case 'import_employee' :
		$allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
		if(in_array($_FILES['file-excel']['type'], $allowedFileType)){
			$data = new Spreadsheet_Excel_Reader($_FILES['file-excel']['tmp_name']);
			$hasildata = $data->rowcount($sheet_index = 0);
			for ($i = 2; $i <= $hasildata; $i++) {
				$sql = "select employee_code from aismartual_employee	
				where lower(employee_code) = '".strtolower(trim($data->val($i, 2)))."' and 
				lower(employee_name) = '".strtolower(trim($data->val($i, 3)))."'";
				$exe = mysqli_query($connDB, $sql);
				writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);
				$result = mysqli_fetch_array($exe, MYSQLI_ASSOC);

				if($result['employee_uuid'] == ''){
					$sql = "insert into aismartual_employee (employee_uuid, employee_code, employee_name, status, created_date) 
					values ('".guid()."', '".convertText('upper', trim($data->val($i, 2)))."', '".convertText('upper', trim($data->val($i, 3)))."', 'active', NOW())";
					$exe = mysqli_query($connDB, $sql);
					writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);
				}
			}
			$result = ($exe == true) ? array('message' => 'New Employee imported successfully !!', 'error' => false) : array('message' => 'Error: Data failed to import !!', 'error' => true); 
		}
		else{
			$result = array('message' => 'Error: Invalid File Type !', 'error' => true);
		}
		echo json_encode($result);
	break;
	case 'add_employee' :
		extract($_POST);
		if(empty($isEdit) || $isEdit == ''){
			$sql = "insert into schedule_employee (emp_id, emp_number, emp_name, status, created_at) values ('".guid()."', '".convertText('upper', $emp_number)."', '".convertText('upper', $emp_name)."', '".$isActive."', NOW())";
		}
		// else{
		// 	$sql = "update schedule_employee set emp_number = '".convertText('upper', $employee_code)."', 
		// 	emp_name = '".convertText('upper', $employee_name)."', status = '".$isActive."' 
		// 	where emp_id = '".$isEdit."'";	
		// }

		$exe = mysqli_query($connDB, $sql);
		writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);
		writeLog("insert into schedule_employe (emp_id, emp_number, emp_name, status, created_date) values ('".guid()."', '".convertText('upper', $employee_code)."', '".convertText('upper', $employee_name)."', '".$isActive."', NOW())")
		$result = ($exe == true) ? array('message' => 'New Employee '.((empty($isEdit) || $isEdit == '') ? 'added' : 'updated').' successfully !!', 'error' => false) : array('message' => 'Error: Data failed to save !!', 'error' => true); 
		echo json_encode($result);	
	break;
	case 'loginApp' :
		// login($_POST['userId'], base64_encode(md5(trim($_POST['password']))));
		login($_POST['userId'], $_POST['password']);
	break;
}

#:---------------------------------------------------------------------------------------------------------------------------:#
#:---------------------------------------------------------------------------------------------------------------------------:#
#:---------------------------------------------------------------------------------------------------------------------------:#

switch($_GET['act']){
	case 'deleteData':
		$sql = "delete from ".$_GET['table']." where ".$_GET['param']." = '".$_GET['id']."'";
		$exe = mysqli_query($connDB, $sql);
		writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);
		
		$result = ($exe == true) ? array('message' => 'Data has been deleted successfully !!', 'error' => false) : array('message' => 'Error: Data Failed to delete !!', 'error' => true);	
		echo json_encode($result);
	break;
	case 'getActiveData' :
		$split = explode("#", $_POST['param']);	
		$nilai = ($split[1] == 'active') ? 'inactive' : 'active';
		$sql 	= "update ".$_POST['table']." set status = '".$nilai."' where ".$split[2]." = '".$split[0]."'";
		$exe 	= mysqli_query($connDB, $sql);
		writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);
		$result = ($exe == true) ? array('error' => false) : array('error' => true);
		echo json_encode($result);
	break;
	case 'signout' :
		$sql = "update schedule_user_management set online_status = '0' where user_id = ".$_SESSION['kdUser'];
		$exe = mysqli_query($connDB, $sql);
		writeLog(__LINE__, __FILE__, mysqli_error($connDB), $sql);

		session_unset();
		session_destroy();
		$result  =  array('message'  => 'Thank You !!', 'error'  => false);
		echo json_encode($result);
	break;
	case 'getProductList' :
		$sql = "select product_list_uuid, concat(product_id,' / ', product_old_code) product_name from aismartual_product_list where is_archived = '0' order by product_id";
		$exe = mysqli_query($connDB, $sql);
		$x=0;
		while($row = mysqli_fetch_array($exe)){
			$x++;
			$result[] = array(
				'id' 		=> $row['product_list_uuid'],
				'text' 	=> $x.". ".$row['product_name']
		  	);
		}
		echo json_encode($result);
	break;
	case 'unsetInputParam' :
		unset($_SESSION['name_entry'], $_SESSION['location_entry']);
	break;
}
?>
								