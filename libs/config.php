<?php
error_reporting(0);
session_start();
clearstatcache();
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT" );
header("Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT" );
header("Cache-Control: no-cache, must-revalidate" );
header("Pragma: no-cache");
header("Cache-Control: no-cache");

function base_url(){
	$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	$domainName = $_SERVER['HTTP_HOST'].'/aismartual_stock/';
	return $protocol.$domainName;
}

define('base_url', base_url());
define('_HOST', 'localhost');
define('_USER', 'root');
define('_PASS', '');
// define('_DB', 'aismartual_stock');
define('_DB', 'schedule_db');

$connDB = mysqli_connect(_HOST, _USER, _PASS, _DB);	
if (mysqli_connect_errno())	{		
	echo "<b>Connection failed to DB - Error Message: [" . mysqli_connect_error($connDB) . "]</b>";
	exit;
}

global $connDB;	
$_SESSION['base_url'] 	= base_url;

// Sengaja gk di tutup tag PHP nya
