<li><a href="home.php?ref=input-qty-product&parent=product"><i class="fa fa-pencil" aria-hidden="true"></i> <span>Input Quantity</span></a></li>
<li><a href="home.php?ref=stock-report&parent=reporting"><i class='fa fa-table'></i> <span>Stock Report</span></a></li>
<li><a href="home.php?ref=summary-report&parent=reporting"><i class="fa fa-list-alt" aria-hidden="true"></i> <span>Summary Report</span></a></li>
<li class="treeview">
	<a href="#"><i class="fa fa-cogs" aria-hidden="true"></i> <span>Master Data</span><i class="fa fa-angle-left pull-right"></i></a>
	<ul class="treeview-menu">
		<li><a href="home.php?ref=list-product-items&parent=master-data"><i class='fa fa-list'></i> <span>Product Items</span></a></li>
		<li><a href="home.php?ref=manage-employee&parent=master-data"><i class='fa fa-users'></i> <span>Employee</span></a></li>
		<li><a href="home.php?ref=manage-location&parent=master-data"><i class='fa fa-map'></i> <span>Location</span></a></li>
		<li><a href="home.php?ref=manage-user&parent=master-data"><i class='fa fa-circle-o'></i> <span>Manage User</span></a></li>
	</ul>
</li>
<li><a href="home.php?ref=change-pass"><i class="fa fa-unlock-alt" aria-hidden="true"></i> <span>Change Password</span></a></li>
