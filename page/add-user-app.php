<script type="text/javascript">
   $(document).ready(function(){
      var userAcc =  $('#userAccess').val();
      if(userAcc != "") $("#pemeriksaanLab").show(); else $("#pemeriksaanLab").hide();

      $('#cancel').click(function(){ 
         window.location.href='home.php?ref=manage-user&parent=configuration';
      });
   
      $('#email').blur(function(){
         var email = $('#email').val();
         checkDataEntry('diamond_user_management','email', email);
      });
      
      $('#userid').blur(function(){
         var userid = $('#userid').val();
         checkDataEntry('diamond_user_management', 'userid', userid);
      });

		/* ----------------- Check Re-Password -------------------- */
		$('#repassword').blur(function(){
			var passwd 		= $('#password').val();
         var rePasswd 	= $('#repassword').val();
         if(passwd != rePasswd){
            bootbox.alert('Sorry, the password you entered is not the same !');
         }
		});
		
		/* ----------------- Save Data -------------------- */
		$("#form").submit(function() {
			$.ajax({  
				type	   : 'POST',
				url		: $(this).attr('action'),
				data	   : $(this).serialize(),
				dataType : "json",
				success  : function(data) {
					if(data.error == false){
						var timeout = 2000; // 1 seconds
                  var dialog = bootbox.dialog({
                        message : '<p class="text-center">'+ data.message +'</p>',
                        size    : "small",
                        closeButton: false
                  });
                  setTimeout(function () {
                        dialog.modal('hide');
                        location.href='home.php?ref=manage-user';
                  }, timeout);
					}
					else{
                  bootbox.alert(data.message);	
					}
				},  
				error : function() {  
               bootbox.alert('Sorry, a system error occurred, please check LogFiles !!');
				}  
			});
			return false;  
		});
   });
   
   function validateEmail(sEmail) {
      var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
      if (filter.test(sEmail)) {
         return true;
      }
      else {
         return false;
      }
   }

   function checkDataEntry(param, data, scrollTop){
      if(data != ''){
         var catMsg, errorMsg;
         if(param == 'email'){
            if(!validateEmail(data)){
               bootbox.alert('Sorry, please check your email writing form again !');
               return;
            }	
         }
         
         $.ajax({ 
            type	: 'POST',
            url		: '<?=base_url?>libs/proses.php?act=cekValidasiData&param='+ param +'&data='+ data,
            dataType: "json",
            success : function(data) { 
               if(data.error == true){
                  bootbox.alert(data.errorMsg);
                  $('#submit').attr('disabled', true);
               }
               else{
                  $('#submit').attr('disabled', false);
               }
            }
         });
      }
   }
</script>
<?php
if(!empty($_GET['id']) || $_GET['id'] != ''){
	$sql = "select kd_user, userid, password, username, email, kd_access, status from aismartual_user_management where kd_user = ".$_GET['id'];
	$exe = mysqli_query($connDB, $sql);
	$row = mysqli_fetch_array($exe, MYSQLI_ASSOC);
	writeLog(__LINE__, __FILE__, mysqli_error($connDB));
	
	$userid     = $row['userid'];
	$username	= $row['username'];
	$email 		= $row['email'];
   $userAccess	= $row['kd_access'];
   $checked 	= ($row['status'] == 'active') ? "checked" : "";
   $txtPass 	= "";
   $isReadOnlyPass = "";
   $isReadOnlyUser = "readonly";
   $isEdit     = $_GET['id'];
}
else{
   $checked 	= "checked";
   $txtPass    = "stocksystem@2022";
   $isReadOnlyPass = "readonly";
   $isReadOnlyUser = "";
}
?>
<form id="form" name="form" method="post" action="<?=base_url?>libs/proses.php" autocomplete="off">
<div class="center-block col-sm-8" style="padding-left:0px;">
   <div class="panel panel-default">
      <div class="panel-heading">
         <div class="panel-title">
            <i class="fa fa-th-list"></i>
            <b><?php echo (!empty($isEdit)) ? "Update" : "Add New"; ?> User App</b>
         </div>
      </div>
      <div class="panel-body table-responsive">
         <input type="hidden" id="action" name="action" value="add_account">
         <input type="hidden" id="isEdit" name="isEdit" value="<?=$_GET['id']?>">
         <div class="space5"></div>
         <h4 class="txtOrange">System Access Information</h4>
         <table class="table table-striped table-condensed">
            <tbody>
               <tr>
                  <td width="30%"><label class="control-label" for="userid">User ID<span style="color:#F00;">*</span></label></td>
                  <td><input class="form-control input-sm" type="text" placeholder="User ID.." id="userid" name="userid" value="<?=$userid?>" <?=$isReadOnlyUser?> ></td>
               </tr>
               <tr>
                  <td><label class="control-label" for="password">Password<span style="color:#F00;">*</span></label></td>
                  <td><input class="form-control input-sm" type="password" placeholder="Password" id="password" name="password" value="<?=$txtPass?>" <?=$isReadOnlyPass?>  >
                  <?php
                  if(!empty($_SESSION['token']) && empty($_GET['id'])) {
                     echo '<small>Default Password : <em><b>stocksystem@2022</b></em></small>';	
                  }
                  ?> 
                  </td>
               </tr>
               <tr>
                  <td><label class="control-label" for="rePassword">Re-Password<span style="color:#F00;">*</span></label>
                  <td><input class="form-control input-sm" type="password" placeholder="Confirm Password" id="repassword" value="<?=$txtPass?>" <?=$isReadOnlyPass?> />
               </td>
               </tr>
               <tr>
                  <td><label class="control-label" for="userAccess">User Access Level<span style="color:#F00;">*</span></label>
                  <td>
                     <select name="userAccess" id="userAccess" class="form-control select2" data-placeholder="Choose User Access Level.." style="width: 50%;" >
                        <option value=""></option>
                        <?php
                        $sql = "select kd_access, nm_access from aismartual_user_access order by 1";
                        $exe = mysqli_query($connDB, $sql);
                        $x=0;
                        while($row = mysqli_fetch_array($exe, MYSQLI_ASSOC)){
                           $x++;
                           $selected = ($row['kd_access'] == $userAccess) ? "selected" : "";
                           echo '<option value="'.$row['kd_access'].'" '.$selected.'>'.$x.'. '.$row['nm_access'].'</option>';
                        } 
                     ?>
                     </select>
                  </td>
               </tr>
            </tbody>
         </table>
         <hr>
         <h4 class="txtOrange">User Information</h4>
         <table class="table table-striped table-condensed">
            <tbody>
               <tr>
                  <td width="30%"><label class="control-label" for="username">User Name</label></td>
                  <td><input class="form-control input-sm" type="text" placeholder="User Name.." id="username" name="username" value="<?=$username?>"></td>
               </tr>
               <tr>
                  <td><label class="control-label" for="email">Email</label></td>
                  <td><input class="form-control input-sm" type="text" placeholder="Email Address.." id="email" name="email" value="<?=$email?>">
                  <small>&nbsp;e.g : someone@example.com</small></td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td><input class="input-status" type="checkbox" id="isActive" name="isActive" value="active" <?=$checked?> > <label class="control-label" for="inputKategori">Is Active</label></td>
               </tr>
            </tbody>
         </table>
      </div>
      <div class="panel-footer text-right"> 
         <button type="reset" id="cancel" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-remove"></span> Cancel</button>&nbsp;<button id="submit" type="submit" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-save"></span> Save</button>
      </div>
   </div>
</div>    
</form>
</body>
</html>
