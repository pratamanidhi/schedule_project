<script src="<?=base_url?>libs/bootstrap/extend/bootstrap-material-design/js/mdb.min.js"></script>
<div class="center-block">
	<div class="row">
		<div class="col-lg-12 col-xs-12">
			<div class='alert alert-info alert-dismissible fade in' role='alert'>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?php
				echo '<h5><i class="icon fa fa-user"></i> <a style="margin-right:10px; text-decoration:none;">Welcome to Stock Management System - LEE SOON SENG PLASTIC INDUSTRIES SDN BHD <i class="fa fa-smile-o" aria-hidden="true"></i></a></h5>';
				?>
			</div>
		</div>
	</div>	
	<div class="row">
		<div class="col-lg-12 col-xs-12">
			<div class="box box-success">
				<div class="box-header">
					<i class="fa fa-tachometer" aria-hidden="true"></i>
					<h3 class="box-title">Dashboard</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
					</div>
				</div>
				<div class="box-body chat" id="chat-box">
					<div class="col-md-12">
						
						<div class="col-lg-3">
							<!-- small box -->
							<div style="background-color:#f39c12;color:#fff" class="small-box">
								<div class="inner">
									<?php
									
									$sql = "select sum(stock_qty) stock from  aismartual_stock_qty where date(verified_date) = date(CURRENT_DATE()) and month(verified_date) = month(CURRENT_DATE()) and year(verified_date) = year(CURRENT_DATE())  and stock_qty > 0 and is_verified = '1' group by product_id";
									$exe = mysqli_query($connDB, $sql);
									$row = mysqli_fetch_array($exe, MYSQLI_ASSOC);
									$stock = $row['stock'] > 0 ?  $row['stock'] : 0;
									?>
									<h3><?= number_format($stock, 0, ".", ",") ?></h3>
									<p>Stock Today</p>
								</div>
								<div class="icon">
									<i class="fa fa-shopping-bag" aria-hidden="true"></i>
								</div>
								<a href="#" class="small-box-footer" title="Print Report" data-toggle="tooltip"><i class="fa fa-print"></i></a>
							</div>
						</div><!-- ./col -->

						<div class="col-lg-3">
							<!-- small box -->
							<div style="background-color:#00a65a;color:#fff" class="small-box">
								<div class="inner">
									<?php
									$sql = "select sum(stock_qty) stock from aismartual_stock_qty where month(verified_date) = month(CURRENT_DATE()) and year(verified_date) = year(CURRENT_DATE()) and stock_qty > 0 and is_verified = '1' group by product_id";
									$exe = mysqli_query($connDB, $sql);
									$row = mysqli_fetch_array($exe, MYSQLI_ASSOC);
									$stock = $row['stock'] > 0 ?  $row['stock'] : 0;
									?>
									<h3><?= number_format($stock, 0, ".", ",")?></h3>
									<p>Stock This Month</p>
								</div>
								<div class="icon">
									<i class="fa fa-archive" aria-hidden="true"></i>
								</div>
								<a href="#" class="small-box-footer" title="Print Report" data-toggle="tooltip"><i class="fa fa-print"></i></a>
							</div>
						</div><!-- ./col -->
					</div>
				</div>
			</div><!-- /.box (chat box) -->
		</div>
	</div>
</div>
