<script type="text/javascript" language="javascript">
$(document).ready(function() {
   $("#dataTable").on("click", ".isActive", function(){
		var param = $(this).attr('id');
      $.ajax({  
         type	   : 'POST',
			url		: '<?=base_url?>libs/proses.php?act=getActiveData',
			data 		: {'table' : 'aismartual_location', 'param' : param},
			dataType : "json",
			beforeSend: function() {
				$('.loading').css('display', 'block');
			},
         success  : function(data) {
            if(data.error == false){
               location.reload(true);
            }
            else{
               bootbox.alert(data.message);	
            }
         },  
         complete : function(data){
				$('.loading').css('display', 'none');
			}, 
			error : function() {  
				bootbox.alert('Sorry, a system error occurred, please check LogFiles !!');
				$('.loading').css('display', 'none');
			}
      });
      return false;
   });
   
   $("#dataTable").on("click", ".delete", function(){
      var id = $(this).attr('id');
      bootbox.confirm("Are you sure you want to delete this data..?!", function(result) {
		   if(result == true){
            $.ajax({  
               type	   : 'GET',
               url		: '<?=base_url?>libs/proses.php?act=deleteData&table=aismartual_location&param=location_uuid&id='+ id,
					dataType : "json",
					beforeSend: function() {
						$('.loading').css('display', 'block');
					},
               success  : function(data) {
                  if(data.error == false){
                     var timeout = 2000; // 1 seconds
                     var dialog = bootbox.dialog({
                           message : '<p class="text-center">'+ data.message +'</p>',
                           size    : "small",
                           closeButton: false
                     });
                     setTimeout(function () {
                           dialog.modal('hide');
                           location.reload(true);
                     }, timeout);
                  }
                  else{
                     bootbox.alert(data.message);	
                  }
					}, 
					complete : function(){
						$('.loading').css('display', 'none');
					}, 
               error : function() {  
						bootbox.alert('Sorry, a system error occurred, please check LogFiles !!');
						$('.loading').css('display', 'none');
               }  
            });
            return false;  
         }
      });
	});

	$("#form").submit(function() {
		var myForm = document.getElementById('form');
		var formData = new FormData(myForm);
		formData.append("file-excel", $("#file-excel")[0].files[0]);
			
		$.ajax({  
			type			: "POST",
			url			: $(this).attr('action'),
			data			: $(this).serialize(),
			data			: formData,
			contentType	: false,
			processData	: false,
			dataType		: "json",
				beforeSend: function() {
				$('.loading').css('display', 'block');
			},
			success  : function(data) {
				if(data.error == false){
					var timeout = 2000; // 1 seconds
					var dialog = bootbox.dialog({
						message : '<p class="text-center">'+ data.message +'</p>',
						size    : "small",
						closeButton: false
					});
					setTimeout(function () {
						dialog.modal('hide');
						location.reload(true);
					}, timeout);
				}
				else{
					bootbox.alert(data.message);	
				}
			},  
			complete : function(){
				$('.loading').css('display', 'none');
			}, 
			error : function() {  
				bootbox.alert('Sorry, a system error occurred, please check LogFiles !!');
				$('.loading').css('display', 'none');
			}    
		});
		return false;  
	});
	
});
</script>
<div class="center-block">
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title">
					<b>List of Location</b>
					<div class="pull-right">
						<div class="dropdown">
							<button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								<span class="glyphicon glyphicon-plus"></span> Add New
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu">
								<li><a href="home.php?ref=add-new-location&parent=master-data"><i class="fa fa-pencil" aria-hidden="true"></i> Form Input</a></li>
								<li><a href="#" title="Import Location" data-toggle="modal" data-target="#detailModal"><i class="fa fa-upload" aria-hidden="true"></i> Import XLS</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="panel-body table-responsive">
				<table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th width="20%">Location ID</th>
							<th>Location Name</th>
							<th width="15%">Status</th>
							<th width="15%">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$sql = "select location_uuid, location_code, location_name, status from aismartual_location order by 1";		
							$exe = mysqli_query($connDB, $sql);
							writeLog(__LINE__, __FILE__, mysqli_error($connDB));
							$x=0;
							while($row = mysqli_fetch_array($exe, MYSQLI_ASSOC)){
								$x++;
								$isActive 		= ($row['status'] == 'active') ? "Inactive" : "Active";
								$colorActive 	= ($row['status'] == 'active') ? "btn-success" : "btn-warning";
							
								echo '<tr>';	
									echo '<td align="center"><b>'.$x.'</b></td>';
									echo '<td>'.$row['location_code'].'</td>';
									echo '<td>'.$row['location_name'].'</td>';
									echo '<td align="center">';
										echo '<button class="btn btn-sm '.$colorActive.' isActive" id="'.$row['location_uuid']."#".$row['status'].'#location_uuid" alt="isActive"> Set '.$isActive.'</button>';
									echo '</td>';
									echo '<td align="center">';
										echo '<a href="home.php?ref=add-new-location&parent=master-data&id='.$row['location_uuid'].'" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="Update data"><span class="glyphicon glyphicon-pencil"></span></a>';
										echo '<a href="#" class="btn btn-sm btn-danger delete" id="'.$row['location_uuid'].'" data-toggle="tooltip" data-placement="top" title="Delete data"><span class="glyphicon glyphicon-trash"></span></a>';
									echo '</td>';
								echo '</tr>';
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div> 

<!-- Modal -->
<form id="form" name="form" method="post" action="<?=base_url?>libs/proses.php" autocomplete="off" enctype="multipart/form-data">
<input type="hidden" name="action" value="import_location">
	<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailModalTitle" aria-hidden="true">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<h5 class="modal-title" style="font-size: 16px;" id="detailModalTitle">Import Location</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="ace-file-input"> Choose .xls file !</label>
					</div>
					<div class="form-group">
						<input type="file" class="btn btn-primary form-control form-control-sm" accept=".xls" name="file-excel"  id="file-excel">
					</div>
					<div class="form-group">
						<small class="text-warning"> The template file can be downloaded <a href="<?= base_url?>libs/template/location.xls" target="_blank">here</a></small>
					</div>
				</div>
				<div class="panel-footer text-right">
					<button type="reset" id="cancel" class="btn btn-sm btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
					<button type="submit" class="btn btn-sm btn-primary" id="btnUploadFileExcel"><span class="glyphicon glyphicon-upload"></span> Upload</button>
				</div>
			</div>
		</div>
	</div>
</form>