<script type="text/javascript">
   $(document).ready(function(){
      $('#cancel').click(function(){ 
         window.location.href='home.php?ref=stock-report&parent=reporting';
		});
		
		/* ----------------- Save Data -------------------- */
		$("#form").submit(function() {
			$.ajax({  
				type	   : 'POST',
				url		: $(this).attr('action'),
				data	   : $(this).serialize(),
				dataType : "json",
				beforeSend: function() {
					$('.loading').css('display', 'block');
				},
				success  : function(data) {
					if(data.error == false){
						var timeout = 2000; // 1 seconds
                  var dialog = bootbox.dialog({
							message : '<p class="text-center">'+ data.message +'</p>',
							size    : "small",
							closeButton: false
                  });
                  setTimeout(function () {
							dialog.modal('hide');
							location.href='home.php?ref=stock-report&parent=reporting';
                  }, timeout);
					}
					else{
                  bootbox.alert(data.message);	
					}
				},  
				complete : function(){
					$('.loading').css('display', 'none');
				}, 
				error : function() {  
					bootbox.alert('Sorry, a system error occurred, please check LogFiles !!');
					$('.loading').css('display', 'none');
				}
			});
			return false;  
		});
   });
</script>
<?php
if(!empty($_GET['id']) || $_GET['id'] != ''){
   $sql = "select	apl.product_list_uuid, apl.product_id, apl.product_old_code, apl.product_category,	apl.product_status,
	apl.product_uom,	apl.product_total_pack,	asq.stock_qty,	asq.employee_uuid, asq.location_uuid 
	from aismartual_product_list apl left join aismartual_stock_qty asq on apl.product_id = asq.product_id 
	where	apl.product_list_uuid = '".$_GET['id']."'";
   $exe = mysqli_query($connDB, $sql);
   $row = mysqli_fetch_array($exe, MYSQLI_ASSOC);
   writeLog(__LINE__, __FILE__, mysqli_error($connDB));
   extract($row);
}
?>
<form id="form" name="form" method="post" action="<?=base_url?>libs/proses.php" autocomplete="off">
	<div class="center-block col-md-6" style="padding-left:0px; padding-right:0px;">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title">
					<b><?php echo (!empty($product_list_uuid)) ? "Update" : "Input"; ?> Stock Item</b>
				</div>
			</div>
			<div class="panel-body">
				<input type="hidden" id="action" name="action" value="update_stock_item">
				<input type="hidden" id="isEdit" name="isEdit" value="<?= $product_id; ?>">
				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-3 control-label">Person Name <small class="text-red">*</small></label>
						<div class="col-sm-8">
							<select class="form-control select2" id="employee" name="employee" data-placeholder="Choose Person Name.." required>
								<option></option>
								<?php
								$sql = "select employee_uuid, employee_name from aismartual_employee order by employee_name";
								$exe = mysqli_query($connDB, $sql);
								$x=0;
								while($row = mysqli_fetch_array($exe)){
                           $x++;
                           $selected = $row['employee_uuid'] == $employee_uuid ? "selected" : "";
									echo '<option value="'.$row['employee_uuid'].'" '.$selected.'>'.$x.'. '.$row['employee_name'].'</option>';
								}
								?>
							</select>	
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Location <small class="text-red">*</small></label>
						<div class="col-sm-8">
							<select class="form-control select2" id="location" name="location" data-placeholder="Choose Location.." required>
								<option></option>
								<?php
								$sql = "select location_uuid, location_name from aismartual_location order by location_name";
								$exe = mysqli_query($connDB, $sql);
								$x=0;
								while($row = mysqli_fetch_array($exe)){
                           $x++;
                           $selected = $row['location_uuid'] == $location_uuid ? "selected" : "";
									echo '<option value="'.$row['location_uuid'].'" '.$selected.'>'.$x.'. '.$row['location_name'].'</option>';
								}
								?>
							</select>	
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Product ID <small class="text-red">*</small></label>
						<div class="col-sm-8">
							<select class="form-control select2" id="product" name="product" data-placeholder="Choose Product ID.." <?= (!empty($product_list_uuid) ? "disabled" : ""); ?> required>
								<option></option>
								<?php
								$sql = "select product_list_uuid, concat(product_id,' / ', product_old_code) product_name from aismartual_product_list where is_archived = '0' order by product_id";
								$exe = mysqli_query($connDB, $sql);
								$x=0;
								while($row = mysqli_fetch_array($exe)){
                           $x++;
                           $selected = $row['product_list_uuid'] == $product_list_uuid ? "selected" : "";
									echo '<option value="'.$row['product_list_uuid'].'" '.$selected.'>'.$x.'. '.$row['product_name'].'</option>';
								}
								?>
							</select>	
						</div>
               </div>
               <div class="form-group">
						<label class="col-sm-3 control-label">Product Old Code <small class="text-red">*</small></label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="old_code" name="old_code" value="<?= $product_old_code?>" required>
						</div>
               </div>
               <div class="form-group">
						<label class="col-sm-3 control-label">Product Status<small class="text-red">*</small></label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="status" name="status" value="<?= $product_status?>" required>
						</div>
               </div>
               <div class="form-group">
						<label class="col-sm-3 control-label">Product Category<small class="text-red">*</small></label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="category" name="category" value="<?= $product_category?>" required>
						</div>
               </div>
               <div class="form-group">
						<label class="col-sm-3 control-label">Stock UOM<small class="text-red">*</small></label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="uom" name="uom" value="<?= $product_uom?>" required>
						</div>
               </div>
               <div class="form-group">
						<label class="col-sm-3 control-label">Total Pack<small class="text-red">*</small></label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="total_pack" name="total_pack" value="<?= $product_total_pack?>" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Qty <small class="text-red">*</small></label>
						<div class="col-sm-3">
							<div class="input-group">
								<input type="text" class="form-control" id="qty" name="qty" onKeyPress="return isnumber(event);" value="<?= $stock_qty?>" required>
								<span class="input-group-addon">CTN</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel-footer text-right"> 
				<button type="reset" id="cancel" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-remove"></span> Cancel</button>&nbsp;<button id="submit" type="submit" class="btn btn-sm btn-primary submit"><span class="glyphicon glyphicon-save"></span> Save</button>
			</div>
		</div>
	</div>    
</form>
</body>
</html>
