<script type="text/javascript">
   $(document).ready(function(){
      $('#cancel').click(function(){ 
         window.location.href='home.php?ref=manage-location&parent=master-data';
      });

		/* ----------------- Save Data -------------------- */
		$("#form").submit(function() {
			$.ajax({  
				type	   : 'POST',
				url		: $(this).attr('action'),
				data	   : $(this).serialize(),
				dataType : "json",
				beforeSend: function() {
					$('.loading').css('display', 'block');
				},
				success  : function(data) {
					if(data.error == false){
						var timeout = 2000; // 1 seconds
                  var dialog = bootbox.dialog({
							message : '<p class="text-center">'+ data.message +'</p>',
							size    : "small",
							closeButton: false
                  });
                  setTimeout(function () {
							dialog.modal('hide');
							location.href='home.php?ref=manage-location';
                  }, timeout);
					}
					else{
                  bootbox.alert(data.message);	
					}
				},  
				complete : function(){
					$('.loading').css('display', 'none');
				}, 
				error : function() {  
					bootbox.alert('Sorry, a system error occurred, please check LogFiles !!');
					$('.loading').css('display', 'none');
				}
			});
			return false;  
		});
   });
</script>
<?php
$checked = "checked";
if(!empty($_GET['id']) || $_GET['id'] != ''){
	$sql = "select location_uuid, location_code, location_name, status from aismartual_location where location_uuid = '".$_GET['id']."'";
	$exe = mysqli_query($connDB, $sql);
	$row = mysqli_fetch_array($exe, MYSQLI_ASSOC);
	writeLog(__LINE__, __FILE__, mysqli_error($connDB));
	extract($row);
	
	$location_name = convertText('ucwords', $location_name);
   $checked = ($status == 'active') ? "checked" : "";
}
?>
<form id="form" name="form" method="post" action="<?=base_url?>libs/proses.php" autocomplete="off">
	<div class="center-block col-md-6" style="padding-left:0px; padding-right:0px;">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title">
					<b><?php echo (!empty($location_uuid)) ? "Update" : "Add New"; ?> Location</b>
				</div>
			</div>
			<div class="panel-body table-responsive">
				<input type="hidden" id="action" name="action" value="add_location">
				<input type="hidden" id="isEdit" name="isEdit" value="<?= $location_uuid; ?>">
				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-3 control-label">Location ID <small class="text-red">*</small></label>
						<div class="col-sm-8">
							<input class="form-control input-sm" type="text" placeholder="Location ID.." id="location_code" name="location_code" value="<?= $location_code?>" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Location Name <small class="text-red">*</small></label>
						<div class="col-sm-8">
							<input class="form-control input-sm" type="text" placeholder="Location Name.." id="location_name" name="location_name" value="<?= $location_name?>" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">&nbsp;</label>
						<div class="col-sm-4">
							<input class="input-status" type="checkbox" id="isActive" name="isActive" value="1" <?=$checked?>> <label class="control-label" for="inputKategori">Is Active</label>
						</div>
					</div>
				</div>
			</div>
			<div class="panel-footer text-right"> 
				<button type="reset" id="cancel" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-remove"></span> Cancel</button>&nbsp;<button id="submit" type="submit" class="btn btn-sm btn-primary submit"><span class="glyphicon glyphicon-save"></span> Save</button>
			</div>
		</div>
	</div>    
</form>
</body>
</html>
