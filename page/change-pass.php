<script type="text/javascript">
$(document).ready(function(){
	$('#cancel').click(function(){ 
		window.location.href='home.php?ref=home';
	});
	
	$("#form").submit(function() {
        var newPass	    = $("#newPass").val();	
        var reNewPass	= $("#reNewPass").val();
		
		if(newPass != reNewPass){
			bootbox.alert('Sorry, your password repetition input is not the same !!');	
		}
		else{	
			$.ajax({  
				type		: 'POST',
				url		: $(this).attr('action'),
				data		: $(this).serialize(),
				dataType: "json",
				success : function(data) {
					if(data.error == false){
						var timeout = 2000; // 1 seconds
						var dialog = bootbox.dialog({
								message : '<p class="text-center">'+ data.message +'</p>',
								size    : "small",
								closeButton: false
						});
						setTimeout(function () {
							dialog.modal('hide');
						}, timeout);
							
						$.ajax({  
							type    : 'POST',
							url     : '<?=base_url?>libs/proses.php?act=signout',
							dataType: "json",
							success : function(data) {
								var timeout = 1500; // 3 seconds
								setTimeout(function () {
									window.location = '<?=base_url?>';
								}, timeout);
							}
						});
					}
					else{
						bootbox.alert(data.message);
					}
				},  
				error : function() {  
					bootbox.alert("#error");  
				}  
			});
		}
		return false;  
	});

});
</script>
<form id="form" name="form" method="post" action="<?=base_url?>libs/proses.php" autocomplete="off">
<input type="hidden" name="action" value="changePass">
<div class="center-block col-sm-6" style="padding-left:0px;">
    <div class="panel panel-default">
    	<div class="panel-heading">
         <div class="panel-title">
            <i class="fa fa-th-list"></i>
            <b>Change Password</b>
         </div>
        </div>
		<div class="panel-body table-responsive">
			<table class="table table-striped">
				<tr>
					<td><label class="control-label" for="newPass">New Password</label></td>
					<td><input class="form-control" type="password" id="newPass" name="newPass"></td>
                </tr>
                <tr>
					<td><label class="control-label" for="reNewPass">Re-Type New Password</label></td>
					<td><input class="form-control" type="password" id="reNewPass" name="reNewPass"></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><button type="reset" id="cancel" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-remove"></span> Cancel</button>&nbsp;<button type="submit" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-save"></span> Save</button></td>
				</tr>
			</table>
		</div>
	</div>
</div> 
</form>