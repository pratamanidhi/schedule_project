<?php 
error_reporting(0);
session_start(); 
require_once '../libs/config.php'; 
require_once '../libs/phpfunction.php'; 

?>
<script type="text/javascript" language="javascript">
$(document).ready(function() {
   var dataTable = $("#dataTable, .dataTable");
	dataTable.DataTable({
		lengthMenu 		 : [[20, 50, 100, -1], [20, 50, 100, "All"]],
      responsive      : true,
      ordering        : true,
      scrollCollapse  : false,
      paging          : true,
		searching       : true,
      autoFill			 : true,
		dom            : 'frltip',
	});
	
	$("#verify").on("click", function(){
      bootbox.confirm("Are you sure want to Verify the selected data..?!", function(result) {
		   if(result == true){
            var arrData = [];
				$("input:checkbox[name=selectedCheck]:checked").each(function() {
					arrData.push($(this).val());
				});

				$.ajax({  
					type	   : 'POST',
					url		: '<?=base_url?>libs/proses.php',
					data 		: {'action' : 'verify-items', 'data' : arrData},
					dataType : "json",
					beforeSend: function() {
						$('.loading').css('display', 'block');
					},
					success  : function(data) {
                  if(data.error == false){
                     var timeout = 2000; // 1 seconds
                     var dialog = bootbox.dialog({
								message : '<p class="text-center">'+ data.message +'</p>',
								size    : "small",
								closeButton: false
                     });
                     setTimeout(function () {
								dialog.modal('hide');
								location.href='home.php?ref=summary-report&parent=reporting';
                     }, timeout);
                  }
                  else{
                     bootbox.alert(data.message);	
                  }
					}, 
					complete : function(){
						$('.loading').css('display', 'none');
					}, 
               error : function() {  
						bootbox.alert('Sorry, a system error occurred, please check LogFiles !!');
						$('.loading').css('display', 'none');
               }  
				});
            return false;  
         }
      });
	});

	$('#checkAll').click(function(){
		var checked_status = this.checked;
		$(".selected").each(function()
		{
			this.checked = checked_status;
		});
	});

});

</script>
<?php
$where = "";
if(!empty($_POST['start_date']) && !empty($_POST['end_date'])){
   $where .= " and (DATE_FORMAT(asq.entry_date,'%Y-%m-%d') between '".$_POST['start_date']."' and '".$_POST['end_date']."' or DATE_FORMAT(asq.update_date,'%Y-%m-%d') between '".$_POST['start_date']."' and '".$_POST['end_date']."')";
}

if(!empty($_POST['employee'])){
   $where .= " and asq.employee_uuid = '".$_POST['employee']."'";
}

if(!empty($_POST['location'])){
   $where .= " and asq.location_uuid = '".$_POST['location']."'";
}

$sql = "select apl.product_list_uuid, apl.product_id, apl.product_old_code, apl.product_category, apl.product_status,
apl.product_uom, apl.product_total_pack, asq.stock_qty, ae.employee_name, al.location_name, asq.entry_date,
asq.update_date, asq.stock_qty_uuid
from aismartual_product_list apl 
left join aismartual_stock_qty asq on apl.product_id = asq.product_id 
join aismartual_employee ae on asq.employee_uuid = ae.employee_uuid 
join aismartual_location al on asq.location_uuid = al.location_uuid
where asq.stock_qty > 0 and asq.is_verified = '0' ".$where." order by apl.product_id";		
$exe = mysqli_query($connDB, $sql);
writeLog(__LINE__, __FILE__, mysqli_error($connDB));
$row = mysqli_num_rows($exe);
?>
<div class="center-block">
   <?php if(!$exe) : ?>
   <div class="row"> 
		<div class="col-lg-12 col-xs-12">
			<div class='alert alert-warning alert-dismissible fade in' role='alert'>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?php
            echo '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <a style="margin-right:10px; text-decoration:none;">
               Search results cannot be displayed.. An error occurred in the data search filter process.. please check your data search filter input..!!
            </a>';
				?>
			</div>
		</div>
   </div>
   <?php endif; ?>
   <div class="row"> 
		<div class="col-md-12">
			<table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th width="3%">No</th>
						<th>Person Name</th>
						<th>Location Name</th>
						<th width="5%">Product<br>Status</th>
						<th width="5%">Category</th>
						<th>Product ID</th>
						<th>Old Code</th>
						<th width="5%">Stock UOM</th>
						<th width="8%">Total Pack</th>
						<th width="8%">QTY</th>
						<th width="8%">Entry / Update<br>Date</th>
						<th width="8%">Action</th>
						<td width="3%" class='text-center'><input type='checkbox' value='checkAll' name='checkAll' id='checkAll'></td>
					</tr>
				</thead>
				<tbody>
					<?php
						$x=0;
						while($data = mysqli_fetch_array($exe, MYSQLI_ASSOC)){
							$x++;
							echo '<tr>';	
								echo '<td class="text-center"><b>'.$x.'</b></td>';
								echo '<td>'.$data['employee_name'].'</td>';
								echo '<td>'.$data['location_name'].'</td>';
								echo '<td class="text-center">'.$data['product_status'].'</td>';
								echo '<td class="text-center">'.$data['product_category'].'</td>';
								echo '<td>'.$data['product_id'].'</td>';
								echo '<td>'.$data['product_old_code'].'</td>';
								echo '<td class="text-center">'.$data['product_uom'].'</td>';
								echo '<td class="text-right">'.$data['product_total_pack'].'</td>';
								echo '<td class="text-right">'.$data['stock_qty'].'</td>';
								echo '<td class="text-center">'.($data['update_date'] == '0000-00-00 00:00:00' ? $data['entry_date'] : $data['update_date']).'</td>';
								echo '<td align="center">';
									echo '<a href="home.php?ref=update-input-stock&parent=product&id='.$data['product_list_uuid'].'" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Update data"><span class="glyphicon glyphicon-pencil"></span></a>';
								echo '</td>';
								echo '<td class="text-center"><input type="checkbox" value="'.$data['stock_qty_uuid'].'" name="selectedCheck" class="selected"></td>';
							echo '</tr>';
						}
					?>
				</tbody>
			</table>
      </div>
   </div>
</div> 