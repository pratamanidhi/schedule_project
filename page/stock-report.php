<script type="text/javascript" language="javascript">
$(document).ready(function() {
   $("#div-filter-content").hide();

   $("#open-filter-data").click(function(){
      if ( $( "#div-filter-content" ).first().is( ":hidden" ) ) {
         $( "#div-filter-content" ).slideDown( "slow" );
      } else {
         $( "#div-filter-content" ).slideUp( "slow" );
      }
   });

   $("#close-filter-data").click(function(){
      $( "#div-filter-content" ).slideUp( "slow" );
	});
	
	$('#content-list-product').empty().load('<?= base_url;?>page/list-stock.php');

	$("#reset").click(function(){
		$('#start_date, #end_date').val('');
		$('#employee, #location').trigger('change').val(null);
      $('#content-list-product').empty().load('<?= base_url;?>page/list-stock.php');
	});

	/* ----------------- Search Data -------------------- */
	$("#form").submit(function() {
		$.ajax({  
			type	   : 'POST',
			url		: $(this).attr('action'),
			data	   : $(this).serialize(),
			dataType : "html",
			success  : function(data) {
				$('#content-list-product').empty().html(data);
			},  
			error : function() {  
				bootbox.alert('Sorry, a system error occurred, please check LogFiles !!');
			}  
		});
		return false;  
	});
   
});
</script>
<div class="center-block">
   <div class="panel panel-default">
      <div class="panel-heading">
         <div class="panel-title">
				<b>Stock Report</b>
				<div class="pull-right">
					<button type='button' class='btn btn-sm btn-primary' id='verify'><i class="fa fa-check-square-o" aria-hidden="true"></i> Verify</button>
				</div>
         </div>
		</div>
      <div class="panel-body table-responsive">
         <div class="row">
            <div class="col-md-12 text-right">
               <button type="button" class="btn btn-sm btn-default" id="open-filter-data">
                  <i class="fa fa-filter" aria-hidden="true"></i> Filter Data
					</button>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div id="div-filter-content">
                  <div class="bs-callout bs-callout-info" style="padding-bottom:0px;"> 
                     <button type="button" class="close" id="close-filter-data">&times;</button>
                     <h5><i class="fa fa-filter" aria-hidden="true"></i> Filter Data</h5>
							<div class="space10"></div>
							<form id="form" name="form" method="post" action="<?=base_url?>page/list-stock.php" autocomplete="off">
							<table id="participantTable" class="table table-striped table-bordered" style="width:30%;">
									<tbody>
										<tr class="participantRow">
											<td width="8%">
                                    <b>Verify date</b>
											</td>
											<td width="10%">
                                    <input type="text" class="form-control input-sm datepicker" id="start_date" name="start_date" placeholder="Start date.." >
											</td>
                                 <td width="10%">
                                    <input type="text" class="form-control input-sm datepicker" id="end_date" name="end_date" placeholder="End date..">
											</td>
										</tr>
										<tr class="participantRow">
											<td width="8%">
                                    <b>Person Name</b>
											</td>
											<td colspan="2">
												<select class="form-control input-sm select2" id="employee" name="employee" data-placeholder="Choose Person Name.." style="width:100%">
													<option></option>
													<?php
													$sql = "select employee_uuid, employee_name from aismartual_employee order by employee_name";
													$exe = mysqli_query($connDB, $sql);
													$x=0;
													while($row = mysqli_fetch_array($exe)){
														$x++;
														echo '<option value="'.$row['employee_uuid'].'" '.$selected.'>'.$x.'. '.$row['employee_name'].'</option>';
													}
													?>
												</select>
											</td>
										</tr>
										<tr class="participantRow">
											<td width="8%">
                                    <b>Location</b>
											</td>
											<td colspan="2">
												<select class="form-control input-sm select2" id="location" name="location" data-placeholder="Choose Location.."  style="width:100%">
													<option></option>
													<?php
													$sql = "select location_uuid, location_name from aismartual_location order by location_name";
													$exe = mysqli_query($connDB, $sql);
													$x=0;
													while($row = mysqli_fetch_array($exe)){
														$x++;
														echo '<option value="'.$row['location_uuid'].'" '.$selected.'>'.$x.'. '.$row['location_name'].'</option>';
													}
													?>
												</select>
											</td>
										</tr>
										<tr>
											<td colspan="5">
												<div class="pull-left">
													<button class="btn btn-default btn-sm" id="reset" type="button">
														<i class="fa fa-refresh" aria-hidden="true"></i>
														Reset
													</button>
													<button class="btn btn-info btn-sm" type="submit">
														<i class="fa fa-search"></i>
														Apply Search
													</button>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</form>
                  </div>
               </div>
            </div>
         </div>
         <div class="space10"></div>
         <div class="row">
            <div class="col-md-12">
					<div id="content-list-product"></div>
            </div>
         </div>
      </div>
   </div>
</div> 