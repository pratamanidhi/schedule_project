<script type="text/javascript">
   $(document).ready(function(){
		var jsonObj;

      $('#cancel').click(function(){ 
         $.ajax({  
				type	   : 'GET',
				url		: '<?=base_url?>libs/proses.php?act=unsetInputParam',
				dataType : "html",
				success  : function(data) {
					location.reload(true);
				},  
				error : function() {  
					bootbox.alert('Sorry, a system error occurred, please check LogFiles !!');
					$('.loading').css('display', 'none');
				}
			});
			return false;
		});
		
		$("#employee, #location, #product").change(function() {
			var product = $('#product').val();
			if(product != ''){
				$.ajax({  
					type	   : 'POST',
					url		: '<?=base_url?>libs/proses.php',
					data 		: {'action' : 'getQty', 'product' : product},
					dataType : "json",
					success  : function(data) {
						if(data.error == false){
							$('#total_pack').val(data.pack);
						}
					},  
					error : function() {  
						bootbox.alert('Sorry, a system error occurred, please check LogFiles !!');
						$('.loading').css('display', 'none');
					}
				});
				return false;  
			}
		});

		$.ajax({  
			type	   : 'GET',
			url		: '<?=base_url?>libs/proses.php?act=getProductList',
			dataType : "json",
			async    : false,
			success  : function(data) {
				jsonObj = data;
			},  
			error : function() {  
				bootbox.alert('Sorry, a system error occurred, please check LogFiles !!');
				$('.loading').css('display', 'none');
			}
		});

		$('#product').select2({
			data			: dropdownData(),
			multiple		: false,
			allowClear	: true,
			containerCssClass : "show-hide",
			theme: "bootstrap",
			// creating query with pagination functionality.
			query			: function (data) {
				var pageSize,
					dataset,
					that = this;
				pageSize = 20; // Number of the option loads at a time
				results = [];
				if (data.term && data.term !== '') {
					// HEADS UP; for the _.filter function I use underscore (actually lo-dash) here
					results = _.filter(that.data, function (e) {
						return e.text.toUpperCase().indexOf(data.term.toUpperCase()) >= 0;
					});
				} 
				else if (data.term === '') {
					results = that.data;
				}
				data.callback({
					results	: results.slice((data.page - 1) * pageSize, data.page * pageSize),
					more		: results.length >= data.page * pageSize,
				});
			},
		}); 

		function dropdownData() {
			return _.map(jsonObj, function (data) {
				return {
					id		: data.id,
					text	: data.text,
				};
			});
		}

		/* ----------------- Save Data -------------------- */
		$("#form").submit(function() {
			$.ajax({  
				type	   : 'POST',
				url		: $(this).attr('action'),
				data	   : $(this).serialize(),
				dataType : "json",
				beforeSend: function() {
					$('.loading').css('display', 'block');
				},
				success  : function(data) {
					if(data.error == false){
						var timeout = 2000; // 1 seconds
                  var dialog = bootbox.dialog({
							message : '<p class="text-center">'+ data.message +'</p>',
							size    : "small",
							closeButton: false
                  });
                  setTimeout(function () {
							dialog.modal('hide');
							location.reload(true);
                  }, timeout);
					}
					else{
                  bootbox.alert(data.message);	
					}
				},  
				complete : function(){
					$('.loading').css('display', 'none');
				}, 
				error : function() {  
					bootbox.alert('Sorry, a system error occurred, please check LogFiles !!');
					$('.loading').css('display', 'none');
				}
			});
			return false;  
		});
		
   });
</script>
<form id="form" name="form" method="post" action="<?=base_url?>libs/proses.php" autocomplete="off">
	<div class="center-block col-md-6" style="padding-left:0px; padding-right:0px;">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title">
					<b>Input Quantity</b>
				</div>
			</div>
			<div class="panel-body">
				<input type="hidden" id="action" name="action" value="add_qty">
				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-3 control-label">Entry Date <small class="text-red">*</small></label>
						<div class="col-sm-3">
							<input class="form-control input-lg" type="text" placeholder="Location Name.." id="entry_date" name="entry_date" required readonly value="<?= date('Y-m-d')?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Person Name <small class="text-red">*</small></label>
						<div class="col-sm-8">
							<select class="form-control input-lg select2" id="employee" name="employee" data-placeholder="Choose Person Name.." required>
								<option></option>
								<?php
								$sql = "select employee_uuid, employee_name from aismartual_employee order by employee_name";
								$exe = mysqli_query($connDB, $sql);
								$x=0;
								while($row = mysqli_fetch_array($exe)){
									$x++;
									$selected = (!empty($_SESSION['name_entry']) && $_SESSION['name_entry'] == $row['employee_uuid']) ? "selected" : "";
									echo '<option value="'.$row['employee_uuid'].'" '.$selected.'>'.$x.'. '.$row['employee_name'].'</option>';
								}
								?>
							</select>	
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Location <small class="text-red">*</small></label>
						<div class="col-sm-8">
							<select class="form-control input-lg select2" id="location" name="location" data-placeholder="Choose Location.." required>
								<option></option>
								<?php
								$sql = "select location_uuid, location_name from aismartual_location order by location_name";
								$exe = mysqli_query($connDB, $sql);
								$x=0;
								while($row = mysqli_fetch_array($exe)){
									$x++;
									$selected = (!empty($_SESSION['location_entry']) && $_SESSION['location_entry'] == $row['location_uuid']) ? "selected" : "";
									echo '<option value="'.$row['location_uuid'].'" '.$selected.'>'.$x.'. '.$row['location_name'].'</option>';
								}
								?>
							</select>	
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Product ID <small class="text-red">*</small></label>
						<div class="col-sm-8">
							<input id="product" class="form-control input-lg" data-placeholder="Choose Product ID.." name="product" <?= (!empty($product_list_uuid) ? "readonly" : ""); ?> required />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Total Pack</label>
						<div class="col-sm-3">
							<div class="input-group">
								<input type="text" class="form-control input-lg" id="total_pack" name="total_pack" readonly>
								<span class="input-group-addon">CTN</span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Quantity <small class="text-red">*</small></label>
						<div class="col-sm-3">
							<div class="input-group">
								<input type="text" class="form-control input-lg" id="qty" name="qty" onKeyPress="return isnumber(event);" placeholder="Quantity.." required>
								<span class="input-group-addon">CTN</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel-footer text-right"> 
				<button type="reset" id="cancel" class="btn btn-lg btn-default"><span class="fa fa-undo"></span> Reset</button>&nbsp;<button id="submit" type="submit" class="btn btn-lg btn-primary submit"><span class="glyphicon glyphicon-save"></span> Save</button>
			</div>
		</div>
	</div>    
</form>
</body>
</html>
