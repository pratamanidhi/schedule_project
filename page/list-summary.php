<?php 
error_reporting(0);
session_start(); 
require_once '../libs/config.php'; 
require_once '../libs/phpfunction.php'; 

?>
<script type="text/javascript" language="javascript">
$(document).ready(function() {
   var dataTable = $("#dataTable, .dataTable");
	dataTable.DataTable({
		lengthMenu 		 : [[20, 50, 100, -1], [20, 50, 100, "All"]],
      responsive      : true,
      ordering        : true,
      scrollCollapse  : false,
      paging          : true,
		searching       : true,
      autoFill			 : true,
      dom            : 'Bfrltip',
      buttons         : [
         {
            extend: 'excelHtml5',
            exportOptions: {
               columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            },
            title       : 'Summary Report'
         },
         {   
            extend      	: 'pdfHtml5',
            orientation		: 'landscape',
            pageSize			: 'LEGAL',
            exportOptions	: {
               columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            },
            title          : 'Summary Report',
            customize		: function (doc) {
               doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            },
         },
         {   
            extend      	: 'print',
            orientation 	: 'landscape',
            pageSize    	: 'LEGAL',
            exportOptions	: {
               columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            },
            title          : 'Summary Report',
            customize: function ( win ) {
               $(win.document.body)
                  .css( 'font-size', '10pt');

               $(win.document.body).find( 'table' )
                  .addClass( 'compact' )
                  .css( 'font-size', 'inherit' );
            }
         }
      ]
	});
});

</script>
<?php
$where = "";
if(!empty($_POST['start_date']) && !empty($_POST['end_date'])){
   $where .= " and (DATE_FORMAT(asq.verified_date,'%Y-%m-%d') between '".$_POST['start_date']."' and '".$_POST['end_date']."')";
}

if(!empty($_POST['employee'])){
   $where .= " and asq.employee_uuid = '".$_POST['employee']."'";
}

if(!empty($_POST['location'])){
   $where .= " and asq.location_uuid = '".$_POST['location']."'";
}

$sql = "select apl.product_list_uuid, apl.product_id, apl.product_old_code, apl.product_category, apl.product_status,
apl.product_uom, apl.product_total_pack, asq.stock_qty, ae.employee_name, al.location_name, asq.entry_date,
asq.update_date, asq.stock_qty_uuid, asq.verified_date
from aismartual_product_list apl 
left join aismartual_stock_qty asq on apl.product_id = asq.product_id 
join aismartual_employee ae on asq.employee_uuid = ae.employee_uuid 
join aismartual_location al on asq.location_uuid = al.location_uuid
where asq.stock_qty > 0 and asq.is_verified = '1' ".$where." order by apl.product_id";		
$exe = mysqli_query($connDB, $sql);
writeLog(__LINE__, __FILE__, mysqli_error($connDB));
$row = mysqli_num_rows($exe);
?>
<div class="center-block">
   <?php if(!$exe) : ?>
   <div class="row"> 
		<div class="col-lg-12 col-xs-12">
			<div class='alert alert-warning alert-dismissible fade in' role='alert'>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?php
            echo '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <a style="margin-right:10px; text-decoration:none;">
               Search results cannot be displayed.. An error occurred in the data search filter process.. please check your data search filter input..!!
            </a>';
				?>
			</div>
		</div>
   </div>
   <?php endif; ?>
   <div class="row"> 
		<div class="col-md-12">
			<table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th width="3%">No</th>
						<th>Person Name</th>
						<th>Location Name</th>
						<th width="5%">Product<br>Status</th>
						<th width="5%">Category</th>
						<th>Product ID</th>
						<th>Old Code</th>
						<th width="5%">Stock UOM</th>
						<th width="8%">Total Pack</th>
						<th width="8%">QTY</th>
						<th width="8%">Verify Date</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$x=0;
						while($data = mysqli_fetch_array($exe, MYSQLI_ASSOC)){
							$x++;
							echo '<tr>';	
								echo '<td class="text-center"><b>'.$x.'</b></td>';
								echo '<td>'.$data['employee_name'].'</td>';
								echo '<td>'.$data['location_name'].'</td>';
								echo '<td class="text-center">'.$data['product_status'].'</td>';
								echo '<td class="text-center">'.$data['product_category'].'</td>';
								echo '<td>'.$data['product_id'].'</td>';
								echo '<td>'.$data['product_old_code'].'</td>';
								echo '<td class="text-center">'.$data['product_uom'].'</td>';
								echo '<td class="text-right">'.$data['product_total_pack'].'</td>';
								echo '<td class="text-right">'.$data['stock_qty'].'</td>';
								echo '<td class="text-center">'.$data['verified_date'].'</td>';
							echo '</tr>';
						}
					?>
				</tbody>
			</table>
      </div>
   </div>
</div> 