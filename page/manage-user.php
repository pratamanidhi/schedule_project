<script type="text/javascript" language="javascript">
$(document).ready(function() {
   $("#dataTable").on("click", ".isActive", function(){
      var param = $(this).attr('id');
      $.ajax({  
         type	   : 'POST',
         url		: '<?=base_url?>libs/proses.php?act=getActiveData',
			data 		: {'table' : 'schedule_user_management', param},
         dataType : "json",
         success  : function(data) {
            if(data.error == false){
               location.reload(true);
            }
            else{
               bootbox.alert(data.message);	
            }
         },  
         error : function() {  
            bootbox.alert('Sorry, a system error occurred, please check LogFiles !!');
         }  
      });
      return false;
   });
   
   $("#dataTable").on("click", ".delete", function(){
      var id = $(this).attr('id');
      bootbox.confirm("Are you sure you want to delete this data..?!", function(result) {
		   if(result == true){
            $.ajax({  
               type	   : 'GET',
               url		: '<?=base_url?>libs/proses.php?act=deleteData&table=aismartual_user_management&param=kd_user&id='+ id,
               dataType : "json",
               success  : function(data) {
                  if(data.error == false){
                     var timeout = 2000; // 1 seconds
                     var dialog = bootbox.dialog({
								message : '<p class="text-center">'+ data.message +'</p>',
								size    : "small",
								closeButton: false
                     });
                     setTimeout(function () {
								dialog.modal('hide');
								location.reload(true);
                     }, timeout);
                  }
                  else{
                     bootbox.alert(data.message);	
                  }
               },  
               error : function() {  
                  bootbox.alert('Sorry, a system error occurred, please check LogFiles !!');
               }  
            });
            return false;  
         }
      });
   });
});
</script>
<div class="center-block">
   <div class="panel panel-default">
      <div class="panel-heading">
         <div class="panel-title">
            <i class="fa fa-th-list"></i>
            <b>List of User</b>
            <div class="pull-right">
               <a href="home.php?ref=add-user-app&parent=configuration" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Add New User"><span class="glyphicon glyphicon-plus"></span> Add New User</a>
            </div>
         </div>
      </div>
      <div class="panel-body table-responsive">
         <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
               <tr>
                  <th width="5%">No</th>
                  <th>User ID</th>
                  <th>User Name</th>
                  <th>Access Level</th>
                  <th>Email</th>
                  <th>Register Date</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  $sql = "select a.user_id, a.username_id, a.username, a.email, a.status, 
                        f.access_name, DATE_FORMAT(a.date_create, '%d-%m-%Y %h:%i:%s') date_create
                        from schedule_user_management a, schedule_user_access f
                        where a.user_access = f.access_id
                        order by 1";		
                  $exe = mysqli_query($connDB, $sql);
                  writeLog(__LINE__, __FILE__, mysqli_error($connDB));
                  $x=0;
                  while($row = mysqli_fetch_array($exe, MYSQLI_ASSOC)){
                     $x++;
                     $isActive = ($row['status'] == 'active') ? "Set Inactive" : "Set Active";
                     $colorActive = ($row['status'] == 'active') ? "btn-success" : "btn-warning";

                     echo '<tr>';	
                        echo '<td align="center"><b>'.$x.'</b></td>';
                        echo '<td>'.$row['username_id'].'</td>';
                        echo '<td>'.$row['username'].'</td>';
                        echo '<td>'.$row['access_name'].'</td>';
                        echo '<td>'.$row['email'].'</td>';
                        echo '<td align="center">'.$row['datecreate'].'</td>';
                        echo '<td align="center">';
                           echo '<button class="btn btn-sm '.$colorActive.' isActive" id="'.$row['user_id']."#".$row['status'].'#user_id" alt="isActive" data-toggle="tooltip" data-placement="top" title="Active / Inactive User">'.$isActive.'</button>';
                        echo '</td>';
                        echo '<td align="center">';
                           echo '<a href="home.php?ref=add-user-app&parent=configuration&id='.$row['kd_user'].'" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="Update data"><span class="glyphicon glyphicon-pencil"></span></a>';
                           echo '<a href="#" class="btn btn-sm btn-danger delete" id="'.$row['kd_user'].'" data-toggle="tooltip" data-placement="top" title="Delete data"><span class="glyphicon glyphicon-trash"></span></a>';
                        echo '</td>';
                     echo '</tr>';
                  }
               ?>
            </tbody>
         </table>
      </div>
   </div>
</div> 