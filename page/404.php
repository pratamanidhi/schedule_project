<div class="error-page">
	<h2 class="headline text-yellow"> 404</h2>
	<div class="error-content">
		<h3><i class="fa fa-warning text-yellow"></i> Oops! The page you are referring to was not found.</h3>
		<p>We could not find the page you are referring to. <br>Please check again if the url you entered is correct :)</p>
	</div><!-- /.error-content -->
</div><!-- /.error-page -->