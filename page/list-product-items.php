<script type="text/javascript" language="javascript">
$(document).ready(function() {
   $("#archive").on("click", function(){
      bootbox.confirm("Are you sure want to archive selected data..?!", function(result) {
		   if(result == true){
            var arrData = [];
				$("input:checkbox[name=selectedCheck]:checked").each(function() {
					arrData.push($(this).val());
				});

				$.ajax({  
					type	   : 'POST',
					url		: '<?=base_url?>libs/proses.php',
					data 		: {'action' : 'archive-product-items', 'data' : arrData},
					dataType : "json",
					beforeSend: function() {
						$('.loading').css('display', 'block');
					},
					success  : function(data) {
                  if(data.error == false){
                     var timeout = 2000; // 1 seconds
                     var dialog = bootbox.dialog({
								message : '<p class="text-center">'+ data.message +'</p>',
								size    : "small",
								closeButton: false
                     });
                     setTimeout(function () {
								dialog.modal('hide');
								location.reload(true);
                     }, timeout);
                  }
                  else{
                     bootbox.alert(data.message);	
                  }
					}, 
					complete : function(){
						$('.loading').css('display', 'none');
					}, 
               error : function() {  
						bootbox.alert('Sorry, a system error occurred, please check LogFiles !!');
						$('.loading').css('display', 'none');
               }  
				});
            return false;  
         }
      });
	});

	$('#checkAll').click(function(){
		var checked_status = this.checked;
		$(".selected").each(function()
		{
			this.checked = checked_status;
		});
	});

	$("#form").submit(function() {
		var myForm = document.getElementById('form');
		var formData = new FormData(myForm);
		formData.append("file-excel", $("#file-excel")[0].files[0]);
			
		$.ajax({  
			type			: "POST",
			url			: $(this).attr('action'),
			data			: $(this).serialize(),
			data			: formData,
			contentType	: false,
			processData	: false,
			dataType		: "json",
				beforeSend: function() {
				$('.loading').css('display', 'block');
			},
			success  : function(data) {
				if(data.error == false){
					var timeout = 2000; // 1 seconds
					var dialog = bootbox.dialog({
						message : '<p class="text-center">'+ data.message +'</p>',
						size    : "small",
						closeButton: false
					});
					setTimeout(function () {
						dialog.modal('hide');
						location.reload(true);
					}, timeout);
				}
				else{
					bootbox.alert(data.message);	
				}
			},  
			complete : function(){
				$('.loading').css('display', 'none');
			}, 
			error : function() {  
				bootbox.alert('Sorry, a system error occurred, please check LogFiles !!');
				$('.loading').css('display', 'none');
			}    
		});
		return false;  
	});
});
</script>
<div class="center-block">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title">
					<b>Product Items</b>
					<div class="pull-right">
                  <button class="btn btn-sm btn-primary" title="Import Location" data-toggle="modal" data-target="#detailModal"><i class="fa fa-upload" aria-hidden="true"></i> Add New Product Items</button>
                  <button type='button' class='btn btn-sm btn-danger' id='archive'><i class="fa fa-archive" aria-hidden="true"></i> Archive</button>
					</div>
				</div>
         </div>
         <div class="panel-body table-responsive">
				<table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th width="10%">Product<br>Status</th>
                     <th width="10%">Category</th>
                     <th>Product ID</th>
                     <th>Old Code</th>
                     <th width="10%">Stock UOM</th>
                     <th width="10%">Total Pack</th>
                     <th width="10%">Upload Date</th>
							<td width="10%" class='text-center'><input type='checkbox' value='checkAll' name='checkAll' id='checkAll'></td>
						</tr>
					</thead>
					<tbody>
						<?php
							$sql = "select apl.product_list_uuid, apl.product_id, apl.product_old_code, apl.product_category, apl.product_status, apl.product_uom, 	apl.product_total_pack, apl.upload_date 
							from aismartual_product_list apl where apl.is_archived = '0' order by apl.product_id";		
							$exe = mysqli_query($connDB, $sql);
							writeLog(__LINE__, __FILE__, mysqli_error($connDB));
							$x=0;
							while($row = mysqli_fetch_array($exe, MYSQLI_ASSOC)){
								$x++;
                        
                        echo '<tr>';	
									echo '<td class="text-center"><b>'.$x.'</b></td>';
									echo '<td class="text-center">'.$row['product_status'].'</td>';
                           echo '<td class="text-center">'.$row['product_category'].'</td>';
                           echo '<td>'.$row['product_id'].'</td>';
                           echo '<td>'.$row['product_old_code'].'</td>';
                           echo '<td class="text-center">'.$row['product_uom'].'</td>';
                           echo '<td class="text-right">'.$row['product_total_pack'].'</td>';
									echo '<td class="text-center">'.$row['upload_date'].'</td>';
									echo '<td class="text-center"><input type="checkbox" value="'.$row['product_list_uuid'].'" name="selectedCheck" class="selected"></td>';
								echo '</tr>';
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div> 

<!-- Modal -->
<form id="form" name="form" method="post" action="<?=base_url?>libs/proses.php" autocomplete="off" enctype="multipart/form-data">
	<input type="hidden" name="action" value="import_product_list">
	<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailModalTitle" aria-hidden="true">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<h5 class="modal-title" style="font-size: 16px;" id="detailModalTitle">Import Product List</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="ace-file-input"> Choose .xls file !</label>
					</div>
					<div class="form-group">
						<input type="file" class="btn btn-primary form-control form-control-sm" accept=".xls" name="file-excel"  id="file-excel" required>
					</div>
					<div class="form-group">
						<small class="text-warning"> The template file can be downloaded <a href="<?= base_url?>libs/template/product-list.xls" target="_blank">here</a></small>
					</div>
				</div>
				<div class="panel-footer text-right">
					<button type="reset" id="cancel" class="btn btn-sm btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
					<button type="submit" class="btn btn-sm btn-primary" id="btnUploadFileExcel" ><span class="glyphicon glyphicon-upload"></span> Upload</button>
				</div>
			</div>
		</div>
	</div>
</form>